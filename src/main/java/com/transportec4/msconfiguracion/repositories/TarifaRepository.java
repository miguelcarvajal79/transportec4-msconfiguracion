package com.transportec4.msconfiguracion.repositories;
import com.transportec4.msconfiguracion.models.Tarifa;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface TarifaRepository extends MongoRepository<Tarifa, String> {
    Tarifa findById (Integer id);
    /*List<Tarifa> findByVehiculoId (Integer vehiculo);
    List<Tarifa> findByRutaId (Integer ruta);*/
}
