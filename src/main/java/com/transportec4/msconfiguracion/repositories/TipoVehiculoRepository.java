package com.transportec4.msconfiguracion.repositories;
import com.transportec4.msconfiguracion.models.Ruta;
import com.transportec4.msconfiguracion.models.TipoVehiculo;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface TipoVehiculoRepository extends MongoRepository <TipoVehiculo, String> {
    TipoVehiculo findById (Integer id);
}
