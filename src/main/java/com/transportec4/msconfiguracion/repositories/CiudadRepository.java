package com.transportec4.msconfiguracion.repositories;
import com.transportec4.msconfiguracion.models.Ciudad;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CiudadRepository extends  MongoRepository <Ciudad, String> {
    Ciudad findById (Integer id);
}
