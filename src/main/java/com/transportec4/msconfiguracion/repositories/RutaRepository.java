package com.transportec4.msconfiguracion.repositories;
import com.transportec4.msconfiguracion.models.Ruta;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface RutaRepository extends MongoRepository<Ruta, String> {
    Ruta findById (Integer id);
    List<Ruta> findByCiudadOrigenId (Integer ciudadorigenid);
    List<Ruta> findByCiudadDestinoId (Integer ciudaddestinoid);
}
