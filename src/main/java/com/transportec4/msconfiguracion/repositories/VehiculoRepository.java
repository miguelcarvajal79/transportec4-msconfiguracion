package com.transportec4.msconfiguracion.repositories;

import com.transportec4.msconfiguracion.models.Vehiculo;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;


public interface VehiculoRepository extends MongoRepository <Vehiculo, String> {
    Vehiculo findById (Integer id);
    List<Vehiculo> findBytipovehiculo (Integer tipovehiculo);
    List<Vehiculo> findByusuario (Integer usuario);

}
