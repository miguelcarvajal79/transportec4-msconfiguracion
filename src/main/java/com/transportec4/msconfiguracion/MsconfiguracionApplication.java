package com.transportec4.msconfiguracion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsconfiguracionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsconfiguracionApplication.class, args);
	}

}
