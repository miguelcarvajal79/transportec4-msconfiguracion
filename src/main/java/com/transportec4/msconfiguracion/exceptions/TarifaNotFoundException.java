package com.transportec4.msconfiguracion.exceptions;

public class TarifaNotFoundException extends RuntimeException{
    public TarifaNotFoundException(String message) {
        super(message);
    }
}
