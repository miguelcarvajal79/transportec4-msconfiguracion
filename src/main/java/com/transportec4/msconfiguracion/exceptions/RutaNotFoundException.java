package com.transportec4.msconfiguracion.exceptions;

public class RutaNotFoundException extends RuntimeException {
    public RutaNotFoundException(String message) {
        super(message);
    }
}