package com.transportec4.msconfiguracion.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseBody
public class TipoVehiculoNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(TipoVehiculoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String EntityNotFoundAdvice(TipoVehiculoNotFoundException ex){
        return ex.getMessage();
    }
}
