package com.transportec4.msconfiguracion.exceptions;

public class CiudadNotFoundException extends RuntimeException {
    public CiudadNotFoundException(String message) {
        super(message);
    }
}
