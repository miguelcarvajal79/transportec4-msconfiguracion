package com.transportec4.msconfiguracion.exceptions;

public class TipoVehiculoNotFoundException extends RuntimeException {
    public TipoVehiculoNotFoundException(String message) {
        super(message);
    }
}
