package com.transportec4.msconfiguracion.exceptions;


public class VehiculoNotFoundException extends RuntimeException {
    public VehiculoNotFoundException(String message) {
        super(message);
    }
}