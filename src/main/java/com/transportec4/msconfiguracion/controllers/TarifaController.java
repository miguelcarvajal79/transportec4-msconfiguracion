package com.transportec4.msconfiguracion.controllers;

import com.transportec4.msconfiguracion.exceptions.TarifaNotFoundException;
import com.transportec4.msconfiguracion.exceptions.TipoVehiculoNotFoundException;
import com.transportec4.msconfiguracion.models.Tarifa;
import com.transportec4.msconfiguracion.models.TipoVehiculo;
import com.transportec4.msconfiguracion.repositories.RutaRepository;
import com.transportec4.msconfiguracion.repositories.TarifaRepository;
import com.transportec4.msconfiguracion.repositories.VehiculoRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TarifaController {

    private final TarifaRepository tarifaRepository;
    private final VehiculoRepository vehiculoRepository;
    private final RutaRepository rutaRepository;

    public TarifaController(TarifaRepository tarifaRepository, VehiculoRepository vehiculoRepository, RutaRepository rutaRepository){
        this.tarifaRepository = tarifaRepository;
        this.vehiculoRepository = vehiculoRepository;
        this.rutaRepository = rutaRepository;
    }

    @PostMapping("/tarifas")
    Tarifa TarifaInsert(@RequestBody Tarifa tarifa){
        return tarifaRepository.save(tarifa);
    }

    @GetMapping("tarifas/all")
    List<Tarifa> TarifaAllGet(){
        List<Tarifa> tarifaAll = tarifaRepository.findAll();
        return tarifaAll;
    }

    @GetMapping("tarifas/{tarifaid}")
    Tarifa TarifaGet(@PathVariable Integer tarifaid){
        Tarifa tarifaGet = tarifaRepository.findById(tarifaid);

        if (tarifaGet == null)
            throw new TarifaNotFoundException("No se encontro la tarifa: " + tarifaid.toString());
        return tarifaGet;
    }

    @PutMapping("/tarifas/{tarifaid}")
    Tarifa UpdateTarifa(@RequestBody Tarifa tarifa){
        Tarifa updatetarifa = tarifaRepository.findById(tarifa.getId());
        if (updatetarifa == null){
            throw new TarifaNotFoundException("No se encontro la tarifa: " + tarifa.getId().toString());
        }
        return tarifaRepository.save(tarifa);
    }

    @DeleteMapping("/tarifas/{tarifaid}")
    String DeleteTarifa(@PathVariable Integer tarifa){
        Tarifa deletetarifa = tarifaRepository.findById(tarifa);
        if (deletetarifa == null){
            throw new TarifaNotFoundException("No se encontro la tarifa: " + tarifa.toString());
        }
        tarifaRepository.delete(deletetarifa);
        return "Tipo de tarifa Eliminada";
    }



}
