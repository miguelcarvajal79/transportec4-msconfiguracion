package com.transportec4.msconfiguracion.controllers;




import com.transportec4.msconfiguracion.exceptions.TipoVehiculoNotFoundException;
import com.transportec4.msconfiguracion.models.TipoVehiculo;
import com.transportec4.msconfiguracion.repositories.TipoVehiculoRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TipoVehiculoController {
    private final TipoVehiculoRepository tipoVehiculoRepository;

    public TipoVehiculoController(TipoVehiculoRepository tipoVehiculoRepository){
        this.tipoVehiculoRepository = tipoVehiculoRepository;
    }

    @PostMapping("/tipovehiculos")
    TipoVehiculo tipoVehiculoInsert(@RequestBody TipoVehiculo tipoVehiculo){

        return tipoVehiculoRepository.save(tipoVehiculo);
    }

    @GetMapping("tipovehiculos/all")
    List<TipoVehiculo> TipoVehiculoAllGet(){
        List<TipoVehiculo> tipoVehiculoAll = tipoVehiculoRepository.findAll();
        return tipoVehiculoAll;
    }

    @GetMapping("tipovehiculos/{tipovid}")
    TipoVehiculo TipoVehiculoGet(@PathVariable Integer tipovid){
        TipoVehiculo tipoVehiculoget = tipoVehiculoRepository.findById(tipovid);

        if (tipoVehiculoget == null)
            throw new TipoVehiculoNotFoundException("No se encontro el tipo de vehículo: " + tipovid.toString());

        return tipoVehiculoget;
    }

    @PutMapping("/tipovehiculos/{tipovid}")
    TipoVehiculo UpdatetipoVehiculo(@RequestBody TipoVehiculo tipoVehiculo){
        TipoVehiculo updatetipoVehiculo = tipoVehiculoRepository.findById(tipoVehiculo.getId());
        if (updatetipoVehiculo == null){
            throw new TipoVehiculoNotFoundException("No se encontro el tipo de vehículo: " + tipoVehiculo.getId().toString());
        }
        return tipoVehiculoRepository.save(tipoVehiculo);
    }

    @DeleteMapping("/tipovehiculos/{tipovid}")
    String DeletetipoVehiculo(@PathVariable Integer tipoVehiculo){
        TipoVehiculo deletetipoVehiculo = tipoVehiculoRepository.findById(tipoVehiculo);
        if (deletetipoVehiculo == null){
            throw new TipoVehiculoNotFoundException("No se encontro el tipo de vehículo: " + tipoVehiculo.toString());
        }
        tipoVehiculoRepository.delete(deletetipoVehiculo);
        return "Tipo de Vehículo Eliminado";
    }
}
