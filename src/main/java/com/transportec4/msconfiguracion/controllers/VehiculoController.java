package com.transportec4.msconfiguracion.controllers;

import com.transportec4.msconfiguracion.exceptions.TipoVehiculoNotFoundException;
import com.transportec4.msconfiguracion.exceptions.VehiculoNotFoundException;
import com.transportec4.msconfiguracion.models.TipoVehiculo;
import com.transportec4.msconfiguracion.models.Vehiculo;
import com.transportec4.msconfiguracion.repositories.TipoVehiculoRepository;
import com.transportec4.msconfiguracion.repositories.VehiculoRepository;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class VehiculoController {
    private final VehiculoRepository vehiculoRepository;
    private final TipoVehiculoRepository tipoVehiculoRepository;

    public VehiculoController(VehiculoRepository vehiculoRepository, TipoVehiculoRepository tipoVehiculoRepository){
        this.vehiculoRepository  = vehiculoRepository;
        this.tipoVehiculoRepository  = tipoVehiculoRepository;
    }

    @PostMapping("/vehiculos")
    Vehiculo VehiculoInsert(@RequestBody Vehiculo vehiculo){

        return vehiculoRepository.save(vehiculo);
    }

    @GetMapping("/vehiculos/{vehiculoid}")
    Vehiculo VehiculoGet(@PathVariable Integer vehiculoid){
        Vehiculo vehiculo = vehiculoRepository.findById(vehiculoid);

        if(vehiculo == null){
            throw new VehiculoNotFoundException("No se encuentra el vehículo: " + vehiculoid.toString());
        }

        return vehiculo;
    }

    @GetMapping("/vehiculos/all")
    List<Vehiculo> VehiculoAll(){
        List<Vehiculo> vehiculos = vehiculoRepository.findAll();
        return  vehiculos;
    }

    @PutMapping("/vehiculos/{vehiculoid}")
    Vehiculo UpdateVehiculo(@RequestBody Vehiculo vehiculo){
        Vehiculo updatevehiculo = vehiculoRepository.findById(vehiculo.getId());
        if (updatevehiculo == null){
            throw new VehiculoNotFoundException("No se encuentra el vehículo: " + vehiculo.getId().toString());
        }
        return vehiculoRepository.save(vehiculo);
    }

    @DeleteMapping("/vehiculos/{vehiculoid}")
    String DeleteVehiculo(@PathVariable Integer vehiculoid){
        Vehiculo deletevehiculo = vehiculoRepository.findById(vehiculoid);
        if (deletevehiculo == null){
            throw new VehiculoNotFoundException("No se encuentra el vehículo: " + vehiculoid.toString());
        }
        vehiculoRepository.delete(deletevehiculo);
        return "Vehículo Eliminado";
    }
}
