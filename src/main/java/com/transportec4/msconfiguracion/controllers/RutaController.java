package com.transportec4.msconfiguracion.controllers;
import com.transportec4.msconfiguracion.exceptions.RutaNotFoundException;
import com.transportec4.msconfiguracion.exceptions.CiudadNotFoundException;
import com.transportec4.msconfiguracion.models.Ruta;
import com.transportec4.msconfiguracion.models.Ciudad;
import com.transportec4.msconfiguracion.repositories.RutaRepository;
import com.transportec4.msconfiguracion.repositories.CiudadRepository;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class RutaController {
    private final RutaRepository rutaRepository;
    //private final CiudadRepository ciudadRepository;

    public RutaController(RutaRepository rutaRepository/*, CiudadRepository ciudadRepository*/){
        this.rutaRepository = rutaRepository;
        //this.ciudadRepository = ciudadRepository;
    }

    @PostMapping("/rutas")
    Ruta RutaInsert(@RequestBody Ruta ruta){
        /*Ciudad ciudadorigenid = ciudadRepository.findById(ruta.getCiudadOrigenId()).OrElse(null);
        Ciudad ciudaddestinoid = ciudadRepository.findById(ruta.getCiudadDestinoId()).OrElse(null);

        if (ciudadorigenid == null)
            throw new CiudadNotFoundException("No se encontro la ciudad de origen: " + ruta.getCiudadOrigenId());

        if (ciudaddestinoid == null)
            throw new CiudadNotFoundException("No se encontro la ciudad de destino: " + ruta.getCiudadDestinoId());

         */

        return rutaRepository.save(ruta);
    }

    @PutMapping("/rutas/{rutaid}")
    Ruta RutaUpdate(@RequestBody Ruta ruta) {
        Ruta rutaupdate = rutaRepository.findById(ruta.getId());
        //Ciudad ciudadorigenid = ciudadRepository.findById(rutaupdate.getCiudadOrigenId()).OrElse(null);
        //Ciudad ciudaddestinoid = ciudadRepository.findById(rutaupdate.getCiudadDestinoId()).OrElse(null);

        if (rutaupdate == null)
            throw new RutaNotFoundException("No se encontro la ruta: " + ruta.getId().toString());

        /*if (ciudadorigenid == null)
            throw new CiudadNotFoundException("No se encontro la ciudad de origen: " + ruta.getCiudadOrigenId());

        if (ciudaddestinoid == null)
            throw new CiudadNotFoundException("No se encontro la ciudad de destino: " + ruta.getCiudadDestinoId());

         */

        return rutaRepository.save(ruta);
    }

    @DeleteMapping("rutas/{rutaid}")
    String RutaDelete(@PathVariable Integer rutaid){
        Ruta rutadelete = rutaRepository.findById(rutaid);

        if (rutadelete == null)
            throw new RutaNotFoundException("No se encontro la ruta: " + rutaid.toString());

        rutaRepository.delete(rutadelete);

        return "Ruta eliminada";
    }

    @GetMapping("rutas/{rutaid}")
    Ruta RutaGet(@PathVariable Integer rutaid){
        Ruta rutaget = rutaRepository.findById(rutaid);

        if (rutaget == null)
            throw new RutaNotFoundException("No se encontro la ruta: " + rutaid.toString());

        return rutaget;
    }

    @GetMapping("rutas/all")
    List<Ruta> RutaAllGet(){
        List<Ruta> rutaall = rutaRepository.findAll();

        return rutaall;
    }
}