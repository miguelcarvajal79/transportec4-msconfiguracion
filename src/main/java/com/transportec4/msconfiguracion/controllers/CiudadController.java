package com.transportec4.msconfiguracion.controllers;




import com.transportec4.msconfiguracion.exceptions.CiudadNotFoundException;
import com.transportec4.msconfiguracion.repositories.CiudadRepository;
import com.transportec4.msconfiguracion.models.Ciudad;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class CiudadController {
    private final CiudadRepository ciudadRepository;

    public CiudadController(CiudadRepository ciudadRepository){
        this.ciudadRepository = ciudadRepository;
    }

    @PostMapping("/ciudades")
    Ciudad ciudadInsert(@RequestBody Ciudad ciudad){
        return ciudadRepository.save(ciudad);
    }

    @GetMapping("ciudades/all")
    List<Ciudad> CiudadAllGet(){
        List<Ciudad> CiudadAll = ciudadRepository.findAll();
        return CiudadAll;
    }

    @GetMapping("ciudades/{ciudadid}")
    Ciudad CiudadGet(@PathVariable Integer ciudadid){
        Ciudad ciudadget = ciudadRepository.findById(ciudadid);

        if (ciudadget == null)
            throw new CiudadNotFoundException("No se encontro la ciudad: " + ciudadid.toString());
        return ciudadget;
    }

    @PutMapping("/ciudades/{ciudadid}")
    Ciudad UpdateCiudad(@RequestBody Ciudad ciudad){
        Ciudad updateciudad = ciudadRepository.findById(ciudad.getId());
        if (updateciudad == null){
            throw new CiudadNotFoundException("No se encontro la ciudad: " + ciudad.getId().toString());
        }
        return ciudadRepository.save(ciudad);
    }

    @DeleteMapping("/ciudades/{ciudadid}")
    String DeleteCiudad(@PathVariable Integer ciudad){
        Ciudad deleteciudad = ciudadRepository.findById(ciudad);
        if (deleteciudad == null){
            throw new CiudadNotFoundException("No se encontro la ciudad: " + ciudad.toString());
        }
        ciudadRepository.delete(deleteciudad);
        return "Ciudad Eliminada";
    }
}
