package com.transportec4.msconfiguracion.models;
import org.springframework.data.annotation.Id;

public class Tarifa {
    @Id
    private Integer id;
    private Integer vehiculo;
    private Integer ruta;
    private Integer costo;
    private char estado;


    public Tarifa(Integer id, Integer vehiculo, Integer ruta, Integer costo, char estado) {
        this.id = id;
        this.vehiculo = vehiculo;
        this.ruta = ruta;
        this.costo = costo;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Integer vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Integer getRuta() {
        return ruta;
    }

    public void setRuta(Integer ruta) {
        this.ruta = ruta;
    }

    public Integer getCosto() {
        return costo;
    }

    public void setCosto(Integer costo) {
        this.costo = costo;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
}
