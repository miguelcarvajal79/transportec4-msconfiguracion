package com.transportec4.msconfiguracion.models;
import org.springframework.data.annotation.Id;

public class Ciudad {

    @Id
    private Integer Id;
    private String nombre;

    public Ciudad(Integer id, String nombre) {
        setId(id);
        this.setNombre(nombre);
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
