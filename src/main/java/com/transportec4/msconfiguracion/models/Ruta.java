package com.transportec4.msconfiguracion.models;
import org.springframework.data.annotation.Id;
import java.util.Date;

public class Ruta {
    @Id
    private Integer id;
    private Integer ciudadorigenid;
    private Integer ciudaddestinoid;
    private Date fecha;
    private char estado;

    public Ruta(Integer id,Integer ciudadorigenid,Integer ciudaddestinoid,Date fecha,char estado){
        this.id = id;
        this.ciudadorigenid = ciudadorigenid;
        this.ciudaddestinoid = ciudaddestinoid;
        this.fecha = fecha;
        this.estado = estado;
    }

    public Integer getId(){
        return id;
    }
    public void setId(Integer pid){
        this.id = pid;
    }

    public Integer getCiudadOrigenId(){
        return ciudadorigenid;
    }
    public void setCiudadOrigenId(Integer ciudadorigenid){
        this.ciudadorigenid = ciudadorigenid;
    }

    public Integer getCiudadDestinoId(){
        return ciudaddestinoid;
    }
    public void setCiudadDestinoId(Integer ciudaddestinoid){
        this.ciudaddestinoid = ciudaddestinoid;
    }

    public Date getFecha(){
        return fecha;
    }
    public void setFecha(Date fecha){
        this.fecha = fecha;
    }

    public char getEstado(){
        return estado;
    }
    public void setEstado(char estado){
        this.estado = estado;
    }
}
