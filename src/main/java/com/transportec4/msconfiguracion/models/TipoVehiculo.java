package com.transportec4.msconfiguracion.models;
import org.springframework.data.annotation.Id;

public class TipoVehiculo {
    @Id
    private Integer id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private char estado;

    public TipoVehiculo(Integer id, String codigo, String nombre, String descripcion, char estado) {
        this.setId(id);
        this.setCodigo(codigo);
        this.setNombre(nombre);
        this.setDescripcion(descripcion);
        this.setEstado(estado);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }
}
