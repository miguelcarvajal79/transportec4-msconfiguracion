package com.transportec4.msconfiguracion.models;
import org.springframework.data.annotation.Id;
import java.util.Date;

public class Vehiculo {

    @Id
    private Integer id;
    private String matricula;
    private String modelo;
    private String color;
    private char estado;
    private  char disponibilidad;
    private Date fecha;
    private String empresa;
    private Integer tipovehiculo;
    private Integer usuario;

    public Vehiculo(Integer id, String matricula, String modelo, String color, char estado, char disponibilidad, Date fecha, String empresa, Integer tipovehiculo, Integer usuario) {
        this.id = id;
        this.matricula = matricula;
        this.modelo = modelo;
        this.color = color;
        this.estado = estado;
        this.disponibilidad = disponibilidad;
        this.fecha = fecha;
        this.empresa = empresa;
        this.tipovehiculo = tipovehiculo;
        this.usuario = usuario;
    }


    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public char getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(char disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Integer getTipovehiculo() {
        return tipovehiculo;
    }

    public void setTipovehiculo(Integer tipovehiculo) {
        this.tipovehiculo = tipovehiculo;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }
}
